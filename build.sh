#!/bin/bash

echo "Generating pages..."

rm -rf generated && mkdir generated
rm -rf public

cat src/styles.css > generated/styles.css

# Add URL to head
cat src/url.part     >> src/head.part

cat src/head.part    >  generated/index.html
cat src/index.part   >> generated/index.html
cat src/tail.part    >> generated/index.html

cat src/head.part    >  generated/git.html
cat src/git.part     >> generated/git.html
cat src/tail.part    >> generated/git.html

cat src/head.part    >  generated/docker.html
cat src/docker.part  >> generated/docker.html
cat src/tail.part    >> generated/docker.html

mv generated public

ls -la .
ls -la src/
ls -la public/

echo "Done."
